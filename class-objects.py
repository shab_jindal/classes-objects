#!/usr/bin/env python
# coding: utf-8

# # OOPS-CLASSES AND OBJECTS

# CLASSES ARE BLUEPRINT TO CREATE OBJECTS

# In[42]:


class MyClass:
  x = 5


# In[43]:


p1 = MyClass()
print(p1.x)


# In[85]:


class nme:
    ab = "this is class variable"
    


# In[86]:


obj1 = nme()
obj1.ab


# In[81]:


obj2 = nme()
obj2.ab=("this is updated 1")


# In[84]:


print(obj1.ab, "\n" , obj2.ab)


# In[65]:


class Person:
    
  def __init__(self, name, age): 
#_init_()function is called automatically every time the class is being used to create a new object.


    self.name = name
    self.age = age

p1 = Person("John", 36)
print(p1.name)
print(p1.name,p1.age)
print(p1.age)


# In[ ]:





# Objects can also contain methods. Methods in objects are functions that belong to the object.

# In[55]:


class Person:
  def __init__(self, name, age):
    self.name = name
    self.age = age

  def myfunc(self):
    print("Hello my name is " + self.name,self.age)

p1 = Person("John", 36)
p2 = Person("ayushi",29)
p1.myfunc()
p2.myfunc()


# In[56]:


class fruit:
 def citrus(prop):
    print("sour,vitamin-c,neccessary")
    
f1 = fruit()
fruit.citrus(f1)


# In[41]:


class phone:
    def note(self):
        print("android, 32gb dual-camera")

redmi = phone() #object_name = class_name()
redmi.note()    #object.function()/ class_name.func(object)


# In[ ]:


The self parameter is a reference to the current instance of the class, and is 
used to access variables that belong to the class.



It does not have to be named self , you can call it whatever you like, 
but it has to be the first parameter of any function in the class:


# In[59]:


class Person:
  def __init__(mysillyobject, name, age):
    mysillyobject.name = name
    mysillyobject.age = age

  def myfunc(abc):
    print("Hello my name is " + abc.name,abc.age)

p1 = Person("John", 36)
p1.myfunc()


# # The pass Statement
# class definitions cannot be empty, but if you for some reason have a class definition with no content,
# put in the pass statement to avoid getting an error.
# 
# 

# In[60]:


class customer:
  pass


# In[1]:


class hello:
    pass


# In[4]:


class Person:
  def __init__(self, name, age):
    self.name = name
    self.age = age

  def myfunc(self):
    print("Hello my name is " + self.name,self.age)

p1 = Person("John", 36)
p2 = Person("ayushi",29)
p1.myfunc()
p2.myfunc()


# In[ ]:




